# ingang

This is a piece of middleware intended to help manage invites for [BigBlueButton](https://bigbluebutton.org/) and [Helios])(https://heliosvoting.org/),
the two systems we use in tandem for holding meetings where members can vote.
Its web interface is hosted at
[https://vergadering.bij1.org/ingang/rooms/](https://vergadering.bij1.org/ingang/rooms/).

## requirements
- [Ruby](https://www.ruby-lang.org/)
- [Rake](https://ruby.github.io/rake/)
- [Bundler](https://bundler.io/)

## dependencies

- [rvm](https://rvm.io/)

## usage

```bash
# install ruby version specified in Gemfile
rvm use "ruby-2.7.5"
# create a wrapper for this ruby
PUMA=`gem wrappers show pumactl`

cp config/application.yml.bck config/application.yml.bck
# edit the above file to enter proper values
bundle config set --local path 'vendor'
bundle update
bundle install
bundle binstubs --all
rails app:update:bin
rails credentials:edit
# either restore existing data
cp old/config/master.key ./config/master.key
cp old/db/production.sqlite3 ./db/production.sqlite3
# or scaffold database
rails db:migrate # RAILS_ENV=test
# handle static assets
sudo apt-get update && sudo apt-get install yarn
RAILS_ENV=production rails assets:precompile
rails webpacker:install
# run tests
bin/rake test

# create service
sudo bash -c "cat > /etc/systemd/system/ingang.service" << EOF
[Unit]
Description=Ingang

[Service]
Type=simple
RemainAfterExit=yes
Restart=on-failure
TimeoutSec=300
User=$USER
WorkingDirectory=$PWD
PIDFile=$PWD/tmp/pids/server.pid
ExecStart=$PUMA -F $PWD/config/puma.rb start
ExecStop=$PUMA -F $PWD/config/puma.rb stop
ExecReload=$PUMA -F $PWD/config/puma.rb phased-restart

[Install]
WantedBy=multi-user.target
EOF

# start and check service
sudo systemctl daemon-reload
sudo systemctl stop ingang
sudo systemctl disable ingang
sudo systemctl enable ingang
sudo systemctl start ingang
systemctl status ingang
journalctl -u ingang
```

## todo

- CI
- un-hardcode
  - `app/views/main/stream.html.erb`: video/chat urls

## documentation checklist

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
