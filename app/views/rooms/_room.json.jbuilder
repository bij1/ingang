json.extract! room, :id, :name, :meeting, :attendee_pw, :moderator_pw, :email_subject, :email_body, :created_at, :updated_at
json.url room_url(room, format: :json)
