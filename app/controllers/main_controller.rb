class MainController < ApplicationController
  before_action :set_user_room, only: [:join, :users, :stream]

  def index
  end

  def join
    @running = bbb_server.is_meeting_running?(@room.meeting)
    if not @running and @user.moderator
      meeting = bbb_server.create_meeting(@room.name,
                                          @room.meeting,
                                          { :attendeePW => @room.attendee_pw,
                                            :moderatorPW => @room.moderator_pw })
      @running = meeting[:returncode]
    end

    if @running
      @url = bbb_server.join_meeting_url(@room.meeting,
                                         @user.name,
                                         @user.moderator ? @room.moderator_pw : @room.attendee_pw,
                                         { :userID => @user.id })
      redirect_to @url
    end
  end

  def stream
    start = Time.new(2021, 11, 7, 13, 30, 0, "+01:00")
    @running = false
    if Time.now > start or @user.moderator
      @user.update(presence: true)
      @running = true
    end
  end

  def users
    require 'csv'
    conference = Set.new()
    if @user.moderator
      if bbb_server.is_meeting_running?(@room.meeting)
        bbb_server.get_meeting_info(@room.meeting, @room.moderator_pw)[:attendees].each do |attendee|
          conference.add(attendee[:userID].to_i)
        end
      end
      csv_data = CSV.generate do |csv|
        User.where(room_id: @user.room_id).each do |attendee|
          if not attendee.proxy and (attendee.presence or conference === attendee.id)
            if attendee.vote and voter = attendee
              csv << [voter.id, voter.email, voter.name]
            end
            User.where(email: attendee.email, vote: true, proxy: true, room_id: @user.room_id).each do |proxy|
              csv << [proxy.id, proxy.email, "gemachtigde voor #{proxy.name}"]
            end
          end
        end
      end
    end

    respond_to do |format|
      format.csv { send_data csv_data, filename: "#{@room.name} #{Time.zone.now}.csv" }
    end
  end

  private
    def set_user_room
      @user = User.find_by token: params[:token]
      @room = Room.find(@user.room_id)
    end

end
