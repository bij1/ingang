class ApplicationMailer < ActionMailer::Base
  default from: 'BIJ1 <stemmen@bij1.org>'
  layout 'mailer'
end
