ENV['RAILS_LOG_TO_STDOUT'] = 'yes'
ENV['RAILS_RELATIVE_URL_ROOT'] = '/ingang'

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
