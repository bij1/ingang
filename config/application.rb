require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Ingang
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Default credentials (test-install.blindsidenetworks.com/bigbluebutton).
    config.bigbluebutton_endpoint_default = "https://bigbluebutton3.bij1.org/bigbluebutton/api"

    # Use standalone BigBlueButton server.
    config.bigbluebutton_endpoint = if ENV["BIGBLUEBUTTON_ENDPOINT"].present?
      ENV["BIGBLUEBUTTON_ENDPOINT"]
    else
      config.bigbluebutton_endpoint_default
    end

    config.bigbluebutton_secret = ENV["BIGBLUEBUTTON_SECRET"]

    # Tell Action Mailer to use smtp server, if configured
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address: 'smtp.flowmailer.net',
      port: 587,
      user_name: ENV['SMTP_USERNAME'],
      password: ENV['SMTP_PASSWORD'],
      authentication: 'plain',
      enable_starttls_auto: true }

    config.admin_name = ENV["INGANG_ADMIN_NAME"] || 'admin'
    config.admin_password = ENV["INGANG_ADMIN_PASSWORD"] || ''

    config.hosts << "vergadering.bij1.org"
  end
end
