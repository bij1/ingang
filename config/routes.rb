Rails.application.routes.draw do
  resources :rooms do
    resources :users, shallow: true
  end
  get 'rooms/:room_id/users/bulk', to: 'users#bulk', as: 'bulk_new_room_users'
  post 'rooms/:room_id/users/bulk', to: 'users#create_bulk', as: 'bulk_create_room_users'
  post 'rooms/:room_id/users/:id/test_invite', to: 'users#test_invite', as: 'test_invite_user'
  post 'rooms/:room_id/users/invite', to: 'users#invite', as: 'invite_room_users'
  delete 'rooms/:room_id/users', to: 'users#destroy_all', as: 'destroy_room_users'
  delete 'rooms/:room_id/users/invite', to: 'users#uninvite', as: 'uninvite_room_users'
  post 'rooms/:room_id/users/mark_invited', to: 'users#mark_invited', as: 'mark_invited_room_users'
  get 'rooms/:id/aanwezig.csv', to: 'rooms#present', as: 'room_present_users'
  get ':token/aanwezig.csv', to: 'main#users'
  get ':token/stream', to: 'main#stream'
  get ':token', to: 'main#join', as: 'join_room'
  root 'main#index'
end
