class AddEmailSubjectToRooms < ActiveRecord::Migration[6.0]
  def change
    add_column :rooms, :email_subject, :string
  end
end
